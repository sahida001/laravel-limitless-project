<?php

namespace App\Http\Controllers\Mixed;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ImageController extends Controller {
    public function index(): view {
        $imageData = Image::get();
        return view( 'backend.imageUpload', compact( 'imageData' ) );
    }
    public function imageUpload( Request $request ): RedirectResponse {
        $request->validate( [
            'image' => 'required|image|mimes:jpg,jpeg,png,gif,svg|max:3072',
        ] );
        // $imageName = md5( time().rand() ).'.'.$request->image->extension();
        $imageName = time().'.'.$request->image->extension();
        $request->image->move( public_path( 'images' ), $imageName );
        Image::create( [
            'image_name' => $imageName,
        ] );
        return redirect()->back()->withSuccess( 'You have successfully upload image' )->with( 'image', $imageName );
    }
    public function store( Request $request ) {

    }
    public function show( Image $image ) {
        //
    }
    public function edit( Image $image ) {
        //
    }
    public function update( Request $request, Image $image ) {
        //
    }
    public function destroy( Image $image ) {
        //
    }
}
